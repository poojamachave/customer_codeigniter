<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Customer_model extends CI_Model {

public function __construct() {
		$this->load->database('test');
}

public function insert_data($data) {
		$this->db->insert('customer',$data);
		return true;
}

//function to select all data from customer table
public function get_customer() {
   $query=$this->db->get('customer');
   return $query->result();
}

//function to select particular record from table
 public function show_customer($customer_id) {
   $this->db->select("*");
   $this->db->from('customer');
   $this->db->where('customer_id',$customer_id);
   $query = $this->db->get();
   $result = $query->result();
   return $result;
}

 //function to delete selected record from table 
public function delete_customer($customer_id) {
   $this->db->where('customer_id',$customer_id);
   $this->db->delete('customer');

}

//login 
public function login_model($email, $password)  {  
   $this->db->where('email', $email);  
   $this->db->where('password', $password);  
   $query = $this->db->get('customer');  
       //SELECT * FROM users WHERE username = '$username' AND password = '$password'  
   if($query->num_rows() > 0) {  
    return true;  
  }  
  else {  
    return false;       
  }  
}  

//update customers
public function update_customer($customer_id) {    
  $data=array(
    'name'=>$this->input->Post('name'),
    'company_name'=>$this->input->Post('company_name'),
    'mobile_no'=> $this->input->Post('mobile'),
    'birthdate'=>$this->input->post('birthdate'),
    'email'=>$this->input->Post('email'),
    'status'=>$this->input->Post('status'),
    'gender'=>$this->input->Post('gender'),
    'city'=>$this->input->Post('city'),
    'address'=>$this->input->Post('address'),
    'zip'=>$this->input->Post('zip'),
    'state'=>$this->input->Post('state')
    );
  $this->db->where('customer_id',$customer_id);
  if( $this->db->update('customer',$data)) {
    return true;
  }
  else {
    return false;
  }

}  

/*//pagination 
public function record_count() {
  return $this->db->count_all("customer");
}

public function fetch_data($limit, $start) {
  $this->db->limit($limit, $start);
  $query = $this->db->get("customer");
  if ($query->num_rows() > 0) {
    foreach ($query->result() as $row) {
      $data[] = $row;
    }
    return $data;
  }
  return false;
}*/

/*//search
public function search_model($keyword,$hobbies,$status,$gender,$city,$state) {
$perpage = 2;
$page=0;
$limit_start=$perpage*$page;

$query ="SELECT * FROM customer";
$where_condition=' where 1=1 ';

if(!empty($this->input->get('keyword'))) {
  $where_condition .= "and (name LIKE '%".$keyword."%' OR company_name LIKE '%".$keyword."%' OR mobile_no LIKE '%".$keyword."%'  OR email LIKE '%".$keyword."%' OR address LIKE '%".$keyword."%' OR zip LIKE '%".$keyword."%')";

}
if(!empty($this->input->get('hobby'))) {
  $where_condition .="and (hobbies='".$hobbies."')";

}
if(!empty($this->input->get('status'))) {
  $where_condition .="and (status='".$status."')";
}
if(!empty($this->input->get('gender'))) {
  $where_condition .="and (gender='".$gender."')";
}
if(!empty($this->input->get('city'))) {
  $where_condition .="and (city='".$city."')";
}
if(!empty($this->input->get('state'))) {
  $where_condition .="and (state='".$state."')";
}

$order_by='ORDER BY customer_id DESC Limit '.$limit_start.','.$perpage.' ';
$query .=$where_condition.$order_by;
$query=$this->db->query($query);
return $query->result();
}
*/
}


?>