<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Customer_controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//loading helper to avoid error
		$this->load->helper('url');
		$this->load->library('upload');
		//loading model
		$this->load->model('Customer_model');
	} 
  	

    /*public function search() { 
    	//pagination 
    	$config = array();
        $config["base_url"] = base_url() . "Customer_controller/search/";
        $config["total_rows"] = $this->Customer_model->record_count();
        $config["per_page"] = 2;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["customers"]=$this->Customer_model-> fetch_data($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links(); 

        //search
        if(!empty($this->input->get('keyword') || $this->input->get('hobby') || $this->input->get('status') || $this->input->get('gender') || $this->input->get('city') || $this->input->get('state'))) {
        	$data["customers"] =$this->Customer_model->search_model($this->input->get('keyword'),$this->input->get('hobby'),$this->input->get('status'),$this->input->get('gender'),$this->input->get('city'),$this->input->get('state'));
        }	
         $this->load->view('view',$data);
    }*/

    public function show_customer($export=false) {
    	//$this->load->model('Customer_model');
    	$this->load->model('Search_model');
    	$data['pageTitle']="Customer List";
		$data['title']="Customer List";
		$query_str='';
		$input_get=$this->input->get();
		if(!empty($input_get)) {
			$query_str = http_build_query($input_get);
		}

		/* list pagination config  */
		$this->load->library('pagination');
		$config=get_pagination_config();
        $config['base_url'] = base_url().'Customer_controller/show_customer/?';

        $page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}	

		/* Search customer */
		$keyword=$this->input->get('keyword');
		$hobby=$this->input->get('hobby');
		$status=$this->input->get('status');
		$gender=$this->input->get('gender');
		$city=$this->input->get('city');
		$state=$this->input->get('state');
		$filter=array();

		if($keyword!='') {
				if(strlen($keyword>=3)) {
					$keyword=addslashes($keyword);
					$filter["WHERE"]="(name like '%".$keyword."%' OR company_name like '%".$keyword."%' OR mobile_no like '%".$keyword."%' OR email like '%".$keyword."%' OR address like '%".$keyword."%')";
			}
			$data['clearfilter']=true;
		}
		if($hobby!='') {
			$filter["WHERE"]="(hobbies='".$hobby."')";
			$data['clearfilter']=true;
		}
		if($status!='') {
			$filter["WHERE"]="(status='".$status."')";
			$data['clearfilter']=true;
		}
		if($gender!='') {
			$filter["WHERE"]="(gender='".$gender."')";
			$data['clearfilter']=true;
		}
		if($city!='') {
			$filter["WHERE"]="(city='".$city."')";
			$data['clearfilter']=true;
		}
		if($state!='') {
			$filter["WHERE"]="(state='".$state."')";
			$data['clearfilter']=true;
		}
		
		$count_filter=$filter;
		$count_filter['SELECT']='count(*) as cnt';
		$customer_count=$this->Search_model->get_customer_received_count($count_filter);	
		$config['total_rows'] =$customer_count[0]['cnt'];
		//dsm($config['total_rows']);die;
		$this->pagination->initialize($config);	
        $data['pagination'] = $this->pagination->create_links(); 

        $customer_filter=$filter;
	    if($export!='export') {
	        $customer_filter['LIMIT']=array($config['per_page'],$page);
    	}

        $customer_filter['ORDER_BY']=array('customer.customer_id'=>'desc');
		$data['customers']=$this->Search_model->get_customer($customer_filter);
		//dsm($data['customers']);die;
		$config['page_num']=$page;
		$data['config']=$config;
		//dsm($data['customers']);die;
		//$data['customers']=$this->Search_model->get_customer_received_details($filter);	
        //$data['config']=$config;
		$data['customers']=$this->load->view('view',$data,true);
	
	}

    

	//first open login page
	public function customer() {
		if($this->session->userdata('email') == '')  {   
      		redirect('Customer_controller/login');  
      	}  
		else {
			$this->load->view('customer_view');
		}
	}

	//customer form_validation
	public function Customer_form_validation() {
		//form validation
		$this->load->library('form_validation');  
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('company_name', 'Company_name', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile_no', 'required|min_length[10]');
		$this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
		$this->form_validation->set_rules('hobbies[]', 'Hobbies', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[customer.email]');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('city[]', 'City', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('zip', 'Zip', 'required');
		$this->form_validation->set_rules('state', 'State', 'required');
		//$this->form_validation->set_rules('uploads', 'Uploads', 'required');
         //dsm($_POST);
		//if validation fails
		if ($this->form_validation->run() == FALSE ) {
			  $this->session->set_flashdata('error', validation_errors());
			  $_SESSION['old_post']=$_POST;
              redirect(base_url().'Customer_controller/customer');
        }

		if(!empty($_POST['edit_id'])) {
			//update data
			$this->Customer_model->update_customer($_POST['edit_id']);
			// set flash data
		    $this->session->set_flashdata('update_msg', 'data successfully updated');
			redirect('Customer_controller/show_customer');
		} 
		else {
			  // insert data
			//validation
			$this->form_validation->set_rules('password', 'Password', 'required|max_length[7]');
		    $this->form_validation->set_rules('confirmpassword', 'Confirmpassword', 'matches[password]');
		    if ($this->form_validation->run() == FALSE ) {
		    	$this->session->set_flashdata('error', validation_errors());
		    	$_SESSION['old_post']=$_POST;
		    	redirect(base_url().'Customer_controller/customer');
		    }

		    $upload="./uploads/";
		   
			//file upload
		    $config['upload_path'] =$upload;
	        $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx';
	        $config['max_size']='5M';
	        $this->load->library('upload',$config);   
	        $uploads="uploads/".$_FILES['file_upload']['name'];
	        //check if folder exists
	        if(!file_exists($uploads)) {
	        	mkdir($uploads);
	        }

	         //check directory
		    if (!is_dir($uploads)) {
				mkdir($uploads, 0777, TRUE);
		    }

	        $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('file_upload')) {
                        $this->upload->display_errors();
                }
                else {
                        $Info = $this->upload->data();
                        dsm($Info);die;
                        redirect(base_url().'Customer_controller/customer');
                        
                }
	        //convert array into string
			$hobby_array=implode(',', $this->input->post('hobbies'));
			$city_array=implode(',', $this->input->post('city'));

		    $data=array(
				'name' 			=>$this->input->post('name'),
				'company_name' 	=>$this->input->post('company_name'),
				'mobile_no' 	=>$this->input->post('mobile'),
				'birthdate' 	=>$this->input->post('birthdate'),
				'hobbies'		=>$hobby_array,
				'email' 	    =>$this->input->post('email'),
				'status' 	    =>$this->input->post('status'),
				'gender' 	    =>$this->input->post('gender'),
				'password'		=>$this->input->post('password'),
				'city'      	=>$city_array,
				'address'      	=>$this->input->post('address'),
				'zip'       	=>$this->input->post('zip'),
				'state'      	=>$this->input->post('state'),
				'uploads'      	=>$uploads

			);

			//insert data into database 
			$this->Customer_model->insert_data($data);
			$this->load->view('customer_view',$data);
			}
			// set flash data
			$this->session->set_flashdata('success_msg', 'data successfully inserted');
			redirect('Customer_controller/show_customer');
	}

	//login 
	public function login() {
		$this->load->view('login');
	}

	//login validation
	public function login_validation()  {  
			//login validation
           $this->load->library('form_validation');  
           $this->form_validation->set_rules('email', 'Email', 'required');  
           $this->form_validation->set_rules('password', 'Password', 'required'); 

           //if valid then
           if($this->form_validation->run()) {
                //true  
                $email = $this->input->post('email');  
                $password = $this->input->post('password');  
                //model function  
                $this->load->model('Customer_model');  
                if($this->Customer_model->login_model($email, $password)) {  
                     $session_data = array(  
                          'email'     =>     $email  
                     );  
                     $this->session->set_userdata($session_data);  
                     redirect(base_url() . 'Customer_controller/enter');  
                }  
                else  {  
                     $this->session->set_flashdata('error', 'Invalid Username and Password');  
                     redirect(base_url() . 'Customer_controller/login');  
                }  
           }  
           else {  
                //false  
           		$this->session->set_flashdata('error', validation_errors());
                redirect(base_url() . 'Customer_controller/login');
           }  

    }  

    public  function enter() {  
      	if($this->session->userdata('email') != '') {   
      		echo '<h4>Welcome - '.$this->session->userdata('email').'</h4>';
      		redirect('Customer_controller/customer');  	
      	}  
      	else {  
      		redirect(base_url() . 'Customer_controller/login');  
      	}  
    }  
    
    //logout logged-in users
    public function logout() {  
       $this->session->unset_userdata('email');  
       redirect(base_url() . 'Customer_controller/login');  
    }  

	
	

	// Function to Delete selected record from database.
	public function customer_id_delete() {
		$customer_id=$this->input->get('customer_id');
		$this->Customer_model->delete_customer($customer_id);
		redirect('Customer_controller/customer');
		$this->show_customer_id();
	}

	//function show customer
	public function edit_customer() {
		$customer_id=$this->input->get('customer_id');
		$data['customers']=$this->Customer_model->get_customer();
		$data['customer_id']=$this->Customer_model->show_customer($customer_id);
		$this->load->view('Customer_view',$data);
	}

	//delete customers 
	public function delete_customer() {
		$this->Customer_model->delete($customer_id);
		$query = $this->db->get("customer"); 
		$data['records'] = $query->result(); 
		$this->load->view('view',$data);
	}

}
?>