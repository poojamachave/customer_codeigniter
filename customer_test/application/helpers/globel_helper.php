<?php

	function title_case($value) {
		return ucwords(strtolower(trim($value)));
	}

	function check_loggedin() {
		$CI =& get_instance();
		$user_id = $CI->session->userdata('user_id');
		if(empty($user_id)) {
			set_message('Your session has been expired, Please login again to continue.');
			$backto=str_replace(base_url(),'', current_url());
			redirect(base_url().'?backto='.$backto);
			die;
		}
	}

	/* 
		Message display 
		accept 2 parameter.
		1 - message text.
		2 - (optional) type of message success, error, warning. Bydefault error is selected.
	*/
	function set_message($message,$type="error") {
		$CI =& get_instance();
		if($type=="error") {
			$add=$CI->session->userdata('error');
			$set_message=$add."<li>".$message."</li>";
			$CI->session->set_userdata('error',$set_message);
		}
		else if($type=="success") {
			$add=$CI->session->userdata('success');
			$set_message=$add."<li>".$message."</li>";
			$CI->session->set_userdata('success',$set_message);
		}
		else if($type=="warning") {
			$add=$CI->session->userdata('warning');
			$set_message=$add."<li>".$message."</li>";
			$CI->session->set_userdata('warning',$set_message);
		}  
	}

	function array_mapping($array) {
		$array['month_year']=$array['month'].'-'.$array['year'];
		return $array;
	}


	function week_range($date) {
		$time=strtotime($date);
		$start=date('Y-m-d',strtotime('last monday',$time));
		$end=date('Y-m-d',strtotime('next sunday',$time)); 
		$today=date('l',$time);
		if($today == 'Monday') {
			$start=date('Y-m-d',$time);
		}

		if($today == 'Sunday') {
			$end=date('Y-m-d',$time); 
		}
		return array('start'=>$start,'end'=>$end);
	}	

	/* 
		print array in format 
		accept 1 parameter as array.
	*/

	function dsm($var) {
		if(is_array($var) || is_object($var)) {
			echo "<pre>".print_r($var,true)."</pre>";
		}
		else {
			echo "<pre>".$var."</pre>";
		}
		$debug=debug_backtrace();
		echo "<pre>".$debug[0]['file'].", line :".$debug[0]['line']."</pre><br/>";  
	}

	/* Check Unique value form database  
		$value - Column value to check
		$column - Table column name
		$table - Table name
		$primary - Primary key column of table to ignore check unique value for own row 
		$edit_id  - primary column value to ignore that row for unique check
	*/
	function check_unique($value, $column, $table, $primary_column=false ,$edit_id=false, $company_colum=false, $companyid=false, $filter=false) {
		$CI=&get_instance();

		if(!empty($filter)) {
			apply_filter($filter);
		}

		if(!empty($edit_id)) {
			$CI->db->where($primary_column." !=", $edit_id);
		}

		if(!empty($company_colum) && !empty($companyid)) {
			$CI->db->where($company_colum, $companyid);
		}


		$CI->db->where($column,$value);
		$rs=$CI->db->get($table);
		$result=$rs->result_array();
		if(isset($result[0])) {
			return false;
		}
		else {
			return true;
		}

	
	}	

	/* print last execulated query */
	function print_last_query() {
		$CI =& get_instance();
		dsm($CI->db->last_query());
	}	

	/* 
		redirect back - redirect to request page.
	*/
	function redirect_back() {
		if(isset($_SERVER['HTTP_REFERER'])) {
			$url=$_SERVER['HTTP_REFERER'];  
		}
		else {
			$url=base_url();
		}
		redirect($url);
	}	

	/* 
		CURL Execution
		accept 1 parameter.
		1 - url where page will send the request.
	*/
	function curl_send($url) {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}	

	/* 
		Create combobox from array 
		accept 7 parameter.
		1 - name of select box or combobox.
		2 - array of values.
		3 - option value of select box or combobox.
		4 - text which will display in select box or combobox.
		5 - bydefault selected value in select box or combobox.
		6 - HTML css attributes.
		7 - 'SELECT' will be first option or not.
	*/
	function generate_combobox($name,$array,$key,$value,$selected=false,$other=false,$defaultoption=true) {
		if(empty($array)) {
			$output = "<select name=\"{$name}\" ".$other.">";
			if($defaultoption) {
				$output .= "<option value=\"\">SELECT</option>";    
			}
			$output .= "</select>";
		}
		else{  
			$output = "<select name=\"{$name}\" ".$other.">";
			if($defaultoption) {
				$output .= "<option value=\"\">SELECT</option>";    
			}
			$keys=array_column($array,$key);
			if(is_array($value)) {
				$args=array();
				$args[]="combine";
				foreach($value as $val) {
					$args[]=array_column($array,$val);
				}
				$vals=call_user_func_array('array_map',$args);
			}
			else {
				$vals=array_column($array,$value);
			}

			$new_array=array_combine($keys,$vals);

			foreach ($new_array as $key => $value) {
				if(is_array($selected)) {
					if (in_array($key,$selected)) {
						$output .= "<option value=\"{$key}\" selected>{$value}</option>";
					} 
					else {
						$output .= "<option value=\"{$key}\">{$value}</option>";
					}
				}
				else {
					if ($selected !== false && $selected == $key) {
						$output .= "<option value=\"{$key}\" selected>{$value}</option>";
					} 
					else {
						$output .= "<option value=\"{$key}\">{$value}</option>";
					}
				}
			}

			$output .= "</select>";
		}
		return $output;
	}	

	/* 
		Create combobox from array 
		accept 7 parameter.
		1 - name of select box or combobox.
		2 - array of values.
		3 - option value of select box or combobox.
		4 - text which will display in select box or combobox.
		5 - bydefault selected value in select box or combobox.
		6 - HTML css attributes.
		7 - 'SELECT' will be first option or not.
		8 - array('Attributr name','array or string','saperator') -
			 ex1. array('data-opt',invoice_value')
			 ex2. array('data-opt',array('invoice_value','invoice_numver'),',')
	*/
	function generate_combobox_attr($name,$array,$key,$value,$selected=false,$other=false,$defaultoption=true,$datavalue=false) {
		//dsm(func_get_args());
		if(empty($array)) {
			$output = "<select name=\"{$name}\" ".$other.">";
			if($defaultoption) {
				$output .= "<option value=\"\">SELECT</option>";    
			}
			$output .= "</select>";
		}
		else{  
			$output = "<select name=\"{$name}\" ".$other.">";
			if($defaultoption) {
				$output .= "<option value=\"\">SELECT</option>";    
			}
			$keys=array_column($array,$key);
			$sap_char=' ';			
			if(is_array($value)) {
				$args=array();
				$args[]="combine";
				foreach($value as $val) {
					$args[]=array_column($array,$val);
				}

				/* pass saperator array */
				foreach ($args[1] as $key=>$value) {
					$saperator[$key]=$sap_char;
				}
				$args[]=$saperator;
				$vals=call_user_func_array('array_map',$args);
			}
			else {
				$vals=array_column($array,$value);
			}
			$new_array=array_combine($keys,$vals);

			$opt_attr=array();

			/* for setting extra attribute with dynamic value */
			if($datavalue) {
				/* attribute name */
				$attr=$datavalue[0];
				/* attribute value column */
				$attr_value=$datavalue[1];
				/* create separator array */
				$data_saperator=array();

				/* if multiple column value need to supply */
				if(is_array($attr_value)) {
					/* column value separator  */
					$datasap_char=$datavalue[2];
					$val_columns=array();
					$val_columns[]="combine";

					/* add each column value into val column */
					foreach($attr_value as $attr_val) {
						$val_columns[]=array_column($array,$attr_val);
					}

					/* pass separator array */
					foreach ($attr_value as $key=>$blank) {
						$data_saperator[$key]=$datasap_char;
					}
					$val_columns[]=$data_saperator;

					/* combine multiple column in to string */
					$opt_attr=call_user_func_array('array_map',$val_columns);
					
				}
				else {
					$opt_attr=array_column($array,$attr_value);
				}
			}

			$k=0;
			foreach ($new_array as $key => $value) {
				$str_attr='';
				if($datavalue) {
					$str_attr=' '.$datavalue[0].'="'.$opt_attr[$k].'" ';
				}
				if(is_array($selected)) {
					if (in_array($key,$selected)) {
						$output .= "<option value=\"{$key}\" ".$str_attr." selected>{$value}</option>";
					} 
					else {
						$output .= "<option value=\"{$key}\" ".$str_attr.">{$value}</option>";
					}
				}
				else {
					if ($selected !== false && $selected == $key) {
						$output .= "<option value=\"{$key}\" ".$str_attr." selected>{$value}</option>";
					} 
					else {
						$output .= "<option value=\"{$key}\" ".$str_attr.">{$value}</option>";
					}
				}
				$k++;
			}

			$output .= "</select>";
		}
		return $output;
	}	

	function combine() {
		$args=func_get_args();
		$return='';
		foreach($args as $arg) {
			$return.=$arg.' ';
		}
		$return=rtrim($return,' ');
		 return $return;
	}	

	/* 
		Create textbox from array 
		accept 4 parameters
		1 - name of textbox control.
		2 - value to set in placeholder attribute.
		3 - value to set bydefault in value attribute.
		4 - HTML css attributes.
	*/
	function generate_textbox($name,$placeholder=false,$default=false,$other=false) {
	  $output = "<input type=\"text\" name=\"{$name}\" value=\"{$default}\" placeholder=\"{$placeholder}\" ".$other.">";
	  return $output;
	}

	/* 
		Create number textbox from array 
		accept 4 parameters
		1 - name of numeric textbox control.
		2 - value to set in placeholder attribute.
		3 - value to set bydefault in value attribute.
		4 - HTML css attributes.		
	*/
	function generate_numberbox($name,$placeholder=false,$default=false,$other=false) {
	  $output = "<input type=\"number\" name=\"{$name}\" value=\"{$default}\" placeholder=\"{$placeholder}\" ".$other.">";
	  return $output;
	}

	/* 
		Create file from array
		accept 2 parameters
		1 - name of file control.
		2 - HTML css attributes.	
	*/
	function generate_filebox($name,$other=false) {
	  $output = "<input type=\"file\" name=\"{$name}\" ".$other.">";
	  return $output;
	}

	/* 
		Create textarea from array 
		accept 4 parameters
		1 - name of textarea control.
		2 - value to set in placeholder attribute.
		3 - value to set bydefault in value attribute.
		4 - HTML css attributes.		
	*/
	function generate_textarea($name,$placeholder=false,$default=false,$other=false) {
	  $output = "<textarea name=\"{$name}\" placeholder=\"{$placeholder}\" ".$other.">".$default."</textarea>";
	  return $output;
	}	

	/* 
		Date formatting 
		accept 2 parameters
		1 - date which will be convert to format.
		2 - format in which given date will convert. bydefault false.	
	*/
	function dateformat($date,$format=false) {
		if(!$format) {
			$format="d-m-Y";
		}

		$time=strtotime($date);

		if($time > 0) {
			return date($format,strtotime($date));	
		}
		return '';
		
	}

	function defaultdateformat($date,$format="d M Y") {
		$date_time=strtotime($date);
		$time=date("H:i",$date_time);
		if($time=='00:00') {
			return date($format,$date_time);
		}
		else {
			$format.=" h:i a";
			return date($format,$date_time);
		}
	}

	function mysql_dateformat($str) {
		if(empty($str)) return '';

		$str=str_replace('/', '-', $str);
		$time=strtotime($str);
		if($time) {
			return date('Y-m-d',$time);
		}
		return '';
	}

	function dateformat_PHP_to_momentjs($php_format) {
	    $SYMBOLS_MATCHING = array(
	    	'd' => 'DD',
	        'S' => 'Do',
	        'z' => 'DDD',
	        // Month
	        'm' => 'MM',
	        'M' => 'MMM',
	        'F' => 'MMMM',
	        // Week
	        'W' => 'gg',
	        // Year
	        'Y' => 'YYYY',
	        'y' => 'YY',
	        // Time
	        'h' => 'hh',
	        'H' => 'HH',
	        'i' => 'mm',
	        's' => 'ss',
	    );
	    $jqueryui_format = "";
	    $escaping = false;
	    for($i = 0; $i < strlen($php_format); $i++)
	    {
	        $char = $php_format[$i];
	        if($char === '\\') // PHP date format escaping character
	        {
	            $i++;
	            if($escaping) $jqueryui_format .= $php_format[$i];
	            else $jqueryui_format .= '\'' . $php_format[$i];
	            $escaping = true;
	        }
	        else
	        {
	            if($escaping) { $jqueryui_format .= "'"; $escaping = false; }
	            if(isset($SYMBOLS_MATCHING[$char]))
	                $jqueryui_format .= $SYMBOLS_MATCHING[$char];
	            else
	                $jqueryui_format .= $char;
	        }
	    }
	    return $jqueryui_format;
	}

	/* 
		add minute in time 
		accept 1 parameter
		1 - integer value as minute to be added on current time.
	*/
	function add_time($min) {
		$now = time();
		$add_time = $now + ($min * 60);
		$end_time = date('Y-m-d H:i:s', $add_time);
		return $end_time;
	}

	/* 
		calculate date difference 
		accept 3 parameter
		1 - smaller date.
		2 - larger date.
		3 - type in which difference will return.
	*/
	function datedifference($date1,$date2,$type='dd') {
		$datetime1 = new DateTime($date1);
		$datetime2 = new DateTime($date2);
		$interval = $datetime2->diff($datetime1);
		if($type='dd') {
			return $interval->format('%a');
		}
		elseif($type='mm') {
			return $interval->format('%m');
		}
		elseif($type='yr') {
			return $interval->format('%y');
		}
		elseif($type='hr') {
			return $interval->format('%h');
		}
	}

	/* 
		invoice no. increment
		accept 1 parameter
		1 - last invoice number.		
	*/
	function increment_invoice_no($matches) {
		//return ++$matches[1];
		$val=$matches[1];
		$len=strlen($val);
		++$val;
		return str_pad($val, $len, "0", STR_PAD_LEFT);  
	}	

	function increment_invoice_number($str=false) {
		$CI = get_instance();
		$CI->load->model('invoice_model');		
		/* get company details */
		$company=$CI->company_model->get_companies(array('company.companyid'=>$CI->session->userdata('companyid')));

		/* Invoice Prefix */
		$iprefix=$company[0]['invoice_prefix'];

		/* get last entry of invoice */
		$last_inv=$CI->invoice_model->get_last_inid();
		
		if(!empty($last_inv[0]['invoice_number'])) {

			$invoice_no = increment_string_number($last_inv[0]['invoice_number'], $iprefix);

			while($CI->invoice_model->check_invoice_unique($invoice_no) == false) {
			 	$invoice_no = increment_string_number($invoice_no, $iprefix);
			}

			return $invoice_no;
		}
		else {
			if(!empty($iprefix)) {
				return $iprefix.'-1';
			}
			else {
				return 'INV-1';
			}			
		}
	}


	/* Function used with increment_invoice_number */
	function increment_string_number($str, $iprefix) {
		$matches=explode('-',$str);
		$increment_no=end($matches);
		$len=strlen($increment_no);
		$invoice_no=(int)$increment_no;
		$invoice_no++;

		/* invoice prefix replace date format %m=month / %d=day / %y=year  */
		$iprefix=str_replace('%m',date('m'),$iprefix);
		$iprefix=str_replace('%d',date('d'),$iprefix);
		$iprefix=str_replace('%y',date('Y'),$iprefix);

		if(!empty($iprefix)) {
			return $iprefix.'-'.$invoice_no;
		}	
		else {
			return $invoice_no;
		}
	}

	function increment_quotation_number($str=false) {
		$CI = get_instance();
		$CI->load->model('invoice_model');		
		/* get company details */
		$company=$CI->company_model->get_companies(array('company.companyid'=>$CI->session->userdata('companyid')));
		/* Invoice Prefix */
		$iprefix=$company[0]['quotation_prefix'];


		/* get last entry of invoice */
		$last_inv=$CI->invoice_model->get_last_quid();
		
		
		$last_id=$iprefix.'-0';
		if(!empty($last_inv[0]['invoice_number'])) {
			$last_id=$last_inv[0]['invoice_number'];
		}
		$matches=explode('-',$last_id);
		$increment_no=end($matches);
		$len=strlen($increment_no);
		$quotation_no=(int)$increment_no;
		$quotation_no++;
		/* invoice prefix replace date format %m=month / %d=day / %y=year  */
		$iprefix=str_replace('%m',date('m'),$iprefix);
		$iprefix=str_replace('%d',date('d'),$iprefix);
		$iprefix=str_replace('%y',date('Y'),$iprefix);

		return $quotation_no=$iprefix.'-'.$quotation_no;
		
	}

	/* 
		Increment Number
		accept 1 parameter
		1 - last invoice number.		
	*/
	function increment_invoice_num($number) {
		preg_match('/(.*?)(\d+)$/', $number, $match);
		$base = $match[1];
		$num = $match[2]+1;
	    return $base.$num;  
	}	

	/* 
		Checking string in array.
		accept 2 parameters
		1 - array
		2 - string which is to be search.
	*/
	
	function checkIfInArrayString($array, $searchingFor) {
	    $i=0;
	    foreach ($array as $key=>$element) {
	        if (strpos($element, $searchingFor) !== false) {
	            return array('index'=>$i,'value'=>$key);
	        }
	        $i++;
	    }
	    return false;
	}		

	/* 
		Replaces all spaces with hyphens. & Removes special chars. 
		accept 1 parameter
		1 - string need to be clean.		
	*/
	function clean_name($string) {
	   $string = str_replace('', '-', $string); 
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	}

	/* to remove blank space */
	function clean_field_name($string) {
		$string = preg_replace('/\s+/', ' ',strtolower($string));
	  	$string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
	  	return preg_replace('/[^A-Za-z0-9\_\.]/', '', $string);
	}

	/* 
		Checking image directory & thumb directory
		accept 2 parameters
		1 - name of directory.	
		2 - thumb folder name of directory. bydefault false.
	*/
	function check_dir($name,$thumb=false) {
	    $referer=$_SERVER['HTTP_REFERER'];
	    $server=parse_url($referer);
	    /* main upload folder */
	    if(!file_exists(UPLOAD_DIR) || !is_dir(UPLOAD_DIR)) {
	        mkdir(UPLOAD_DIR);
	    }
	    
	    /* folder of project */
	    if(!file_exists($name) || !is_dir($name)) {
	        mkdir($name);
	    }

	    if($thumb) {
	        /* folder of project */
	        $thumb_path=UPLOAD_DIR.$name.'/'.$thumb;
	        if(!file_exists($thumb_path) || !is_dir($thumb_path)) {
	            mkdir($thumb_path);
	        }     
	    } 
	}

	/* 
		creating the image thumb 
		accept 5 parameters
		1 - source image path.	
		2 - thumb image width.		
		3 - thumb image height.
		4 - original file name.
		5 - thumb folder path where image thumb is to be create.
	*/
	function image_thumb($image_path, $width, $height,$ofilename,$thumb_folder) {
	    $CI =& get_instance();
	    // Path to image thumbnail
	    $image_thumb = $thumb_folder . '/' . $ofilename;
	    if (!file_exists($image_thumb)) {
	        // LOAD LIBRARY
	        $CI->load->library('image_lib');

	        // CONFIGURE IMAGE LIBRARY
	        $config['image_library']    = 'gd2';
	        $config['source_image']     = $image_path;
	        $config['new_image']        = $image_thumb;
	        $config['create_thumb']     = TRUE;
	        $config['maintain_ratio']   = TRUE;
	        $config['width']            = $width;
	        $config['height']           = $height;
	        $CI->image_lib->initialize($config);
	        $CI->image_lib->resize();
	        $CI->image_lib->clear();
	    }
	}	

	/* 
		get thumb image 
		accept 2 parameters
		1 - image path.
		2 - thumb dimension. bydefault false.
	*/
	function get_thumb($image,$thumb=false) {
	    if($image=='') {
	        return "";
	    }
	    $url_array=explode('/',$image);
	    $path_array=pathinfo($image);
	    $last=count($url_array);
	    if($thumb!='') {
	        $url_array[$last]=$path_array['filename'].'_thumb.'.$path_array['extension'];
	        $url_array[$last-1]=$thumb;
	        $thumb_url=implode('/',$url_array);
	    }
	    else {
	        $thumb_url=implode('/',$url_array);
	    }
	    return $thumb_url;
	}				

	/* Generate otp */
    function generateOTP() {
		$password=random_string("numeric",4);
	    return $password;
	}

	/* Generate token */
	function token($length=8) {
		$md5=md5(uniqid(rand(), true));
		return substr($md5,2,$length);
	}		

	/* 
		Parent child array 
		accept 2 parameters
		1 - array.
		2 - parent column name.	
	*/
	function parent_child_array($array,$parent_col) {
		$return = array();
		foreach($array as $key=>$row) {
			$return[$row[$parent_col]][] =$row;
		}
		return $return;
	}	

	function parent_child_array_column($array,$parent_col1, $parent_col2) {
		$return = array();
		foreach($array as $key=>$row) {
			$return[$row[$parent_col1]][$row[$parent_col2]][] =$row;
		}
		return $return;
	}	

	function parent_child_array_column_sum($array,$parent_col1, $parent_col2) {
		$return = array();
		foreach($array as $key=>$row) {
			
			if(empty($return[$row[$parent_col1]][$row[$parent_col2]]['tax_amt'])) {
				$return[$row[$parent_col1]][$row[$parent_col2]]['tax_amt']=0;
			}
			$return[$row[$parent_col1]][$row[$parent_col2]]['tax_amt'] +=$row['tax_amount'];
			
		}
		return $return;
	}

	function parentchildarray($array,$parent_col) {
		$return = array();
		foreach($array as $key=>$row) {
			if (!isset($return[$row[$parent_col]])) {
				$return[$row[$parent_col]] =$row;
				$return[$row[$parent_col]]['child'] =array();
			}
			else {
				$return[$row[$parent_col]]['child'][] =$row;
			}
		}
		return $return;
	}

	/* 
		Replace the text from message 
		accept 2 parameters
		1 - message string.
		2 - array of string that need to be replace.	
	*/
	function replaces($string,$array) {
		foreach($array as $key=>$val) {
			$string=str_replace('|*'.$key.'*|',$val,$string);
		}
		return $string;
	}


	/* 
		Filter for db queries 
		accept 1 parameter
		1 - array of filter.		
	*/
	function apply_filter($filter) {
  $CI=& get_instance();
  if(is_array($filter)) {
      foreach($filter as $key => $val) {
        /* limit */
        if($key==='LIMIT') {
            if(is_array($val)) {
                $CI->db->limit($val[0],$val[1]);
            }
            else {
              $CI->db->limit($val);
            }
        }
        /* for more complex where 
            ex:name='Joe' AND status='boss' OR status='active'
        */
        else if($key==='WHERE') {
          $CI->db->where($val,null,FALSE);
        }
        else if($key==='WHERE_IN') {
           foreach($val as $column => $value) {
              $CI->db->where_in($column,$value);
            }
          
        }
        else if($key==='HAVING') {
          if(is_array($val)) {
            foreach($val as $col=>$value) {
              $CI->db->having($col,$value);
            }
          }
          else {
            $CI->db->having($val,null,FALSE);
          }
        }
        /* order by */
        elseif($key=='ORDER_BY') {
          foreach($val as $col => $order) {
            $CI->db->order_by($col,$order);
          }
        }
        /* group by */
        elseif($key=='GROUP_BY') {
          $CI->db->group_by($val);
        }
        /* select */
        elseif($key=='SELECT') {
          $CI->db->select($val, false);
        }
        /* simple key=>value where condtions */
        else {
          $CI->db->where($key,$val);  
        }
      }
    }
}	

	/* 
		CSV EXPORT 
		accept 2 parameters
		1 - array of record.
		2 - header option.
	*/
	function convertToCSV($data, $options, $name=false) {
		/* setting the csv header*/
		if (is_array($options) && isset($options['headers']) && is_array($options['headers'])) {
			$headers = $options['headers'];
		} 
		else {
			$filename=date('d-M').".csv";
			$headers = array(
				'Content-Type' => 'text/csv',
				'Content-Disposition' => 'attachment; filename="'.$filename.'"'
			);
		}

		$output = '';
		/* setting the first row of the csv if provided in options array */
		if (isset($options['firstRow']) && is_array($options['firstRow'])) {
			$output .= implode(',', $options['firstRow']);
			$output .= "\n"; /* new line after the first line */
		}

		/* setting the columns for the csv. if columns provided, then fetching the or else object keys */
		if (isset($options['columns']) && is_array($options['columns'])) {
			$columns = $options['columns'];
		}
		else {
			if(is_object($data[0])) {
				$objectKeys = get_object_vars($data[0]);
			}
			else {
				$objectKeys = $data[0];	
			}
			$columns = array_keys($objectKeys);
		}

		/* populating the main output string */
		foreach ($data as $row) {
			foreach ($columns as $column) {
				$output .= str_replace(',', ';', $row[$column]);
				$output .= ',';
			}
			$output .= "\n";
		}
		/* $file="./".date('d-m-y').".csv"; */
		$file_name=date('d-m-y').".csv";
		if($name) {
			$file_name=$name;
		}
		/* file_put_contents($file,$output); */
		force_download($file_name, $output);  
	}

	/* 
		Replace the text with database variables 
		accept 2 parameters
		1 - value of database.
		2 - company id optional. bydefault false.
	*/
	function database_variable($variable,$company_id=false) {
		$CI=& get_instance();
		if($company_id!='') {
			$CI->db->where('company_id',$company_id);
		}
		$CI->db->where('key',$variable);
		$rs=$CI->db->get('variable');

		if($rs->num_rows() > 0) {
			$result=$rs->result_array();
			return $result[0]['value'];
		}
		else {
			return false;
		}
	}

	/* 
		Send SMS 
		accept 3 parameters
		1 - mobile no. on which message to be send.
		2 - message.
		3 - company id optional. bydefault false.
	*/
	function send_sms($mobile_no,$message,$company_id=false) {
	    $CI=& get_instance();
		$secure_app_key=database_variable('secure_app_key');
		$secure_sms=database_variable('secure_sms');		
		if($company_id!='') {
			$company_id=$CI->session->userdata('company_id');
			$secure_app_key=database_variable('secure_app_key',$company_id);
			$secure_sms=database_variable('secure_sms',$company_id);
		}
		
	    $message=urlencode($message);
	    /* application based key */
	    /* user based key */
	    $url=SMSAPP_API_SENDSMS."securekey=".$secure_app_key."&token=".$secure_sms."&to=".$mobile_no."&message=".$message;
	    $response=curl_send($url);
	    if(strpos(strtolower($response),'sent') === false) {
	    	return false;
	    }
	    else {
	    	return true;
	    }
	}	

	/* 
		message count 
		accept 2 parameters
		1 - message.
		2 - unicode for other than english language. bydefault 0.	
	*/
	function count_message($text,$unicode=0) {
		$strlen=strlen($text);
		$chars=160;
		if(empty($unicode)) {
			$unicode=0;
		}
		ini_set('default_charset', 'utf-8');
		if($unicode==1) {
			$chars=65;
			$strlen=mb_strlen($text,"UTF-8");
		}

		if($strlen < 1) {
			return 1;
		}
		$count=ceil(($strlen/$chars));
		return $count;
	}

	/* 
		Currency format 
		accept 1 parameter
		1 - integer to be converted into proper format.
	*/
	function moneyFormatIndia($num){
		$explrestunits = "" ;
		if(strlen($num)>3){
			$lastthree = substr($num, strlen($num)-3, strlen($num));
			$restunits = substr($num, 0, strlen($num)-3); /* extracts the last three digits */
			$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; /* explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping. */
			$expunit = str_split($restunits, 2);
			for($i=0; $i<sizeof($expunit); $i++){
				/* creates each of the 2's group and adds a comma to the end */
				if($i==0) {
					$explrestunits .= (int)$expunit[$i].","; /* if is first value , convert into integer */
				}
				else{
					$explrestunits .= $expunit[$i].",";
				}
			}
			$thecash = $explrestunits.$lastthree;
		} 
		else {
			$thecash = $num;
		}
		return $thecash; /* writes the final format where $currency is the currency symbol. */
	}	

	/* 
		get module access for company 
		accept 1 parameter
		1 - Session companyid.
	*/

	function module_access($companyid,$module) {
	    $CI =& get_instance();
	    if($companyid!='') {
	        $CI->load->model('company_model');     
	        $cmodules=$CI->company_model->get_company_modules(array('companyid'=>$companyid, 'modules.module_name'=>$module));
			if(!empty($cmodules)) {
			    return true;
			}
			else {
				return false;
			}
	    }
	    else {
	        return false;
	    }
	}	

	function access_module($user_id) {
	    $CI =& get_instance();
	    if($user_id!='') {
	        $rs_usermodule=$CI->db->query("select module_id from user_module where user_id=?",array($user_id));
	        $user_module=$rs_usermodule->result_array();
	        $CI->load->model('company_model');     
	        $modules=$CI->company_model->get_company_modules(array('companyid'=>$CI->session->userdata('companyid')));
	        $module=array();
	        $access_module=array();
	        $module=array_column($modules,'module_id');
	        foreach ($user_module as $key => $value) {
	            if(in_array($value['module_id'],$module)) {
	              $key_to_access=array_search($value['module_id'],$module);
	              $access_module[]=$modules[$key_to_access]['module_name'];
	            }
	        }
	        return $access_module;
	    }
	    else {
	        return false;
	    }
	}
	/*
		function check_role ($employee_id, $companyid=false) {
			$CI =& get_instance();
			if($employee_id!='') {
				$CI->load->model('employee_model');   
				$role=$CI->employee_model->get_employee_rolecheck(array('employee.employee_id'=>$employee_id, 'employee.companyid'=>$companyid));
		        $rs=$CI->db->query("select title from role where role_id=?",array($role[0]['role_id']));
		        $role_title=$rs->result_array();		
		        return $role_title[0]['title'];
		    }
		    else {
		        return false;
		    }		
		}

		function permission_access($role_id,$permission_id) {
			$CI =& get_instance();
			if($role_id!='' && $permission_id!='') {
		        $rs_rolepermission=$CI->db->query("select * from role_permission where role_id=? and permission_id=?",array($role_id,$permission_id));
		        $count=$rs_rolepermission->num_rows();
		        if($count>0) {
		        	return true;
		        }
		        else {
		        	return false;
		        }
		    }
		    else {
		        return false;
		    }		
		}

		function sum_of_time($times) {
			$minutes=0;
			$seconds=00;
		    // loop throught all the times
		    foreach ($times as $time) {
		        list($hour, $minute) = explode(':', $time);
		        $minutes += $hour * 60;
		        $minutes += $minute;
		    }
		    $hours = floor($minutes / 60);
		    $minutes -= $hours * 60;
		    return sprintf('%02d:%02d:%02d', $hours, $minutes,$seconds);
		}	

		function get_role($role='administrator') {
			$CI =& get_instance();
			$CI->load->model('setting_model'); 
			$role_title=$CI->setting_model->get_companies_roles(array('title'=>$role));
			return $role_title[0]['role_id'];
		}	

		function view_menu_onpermission($role_id) {
		    $CI =& get_instance();
		    if($role_id!='') {
		        $rs_rolepermit=$CI->db->query("select permission_id from role_permission where role_id=?",array($role_id));
		        $rolepermit=$rs_rolepermit->result_array();
		        $CI->load->model('setting_model');     
		        $permission=$CI->setting_model->get_company_permission(array('company_modules.companyid'=>$CI->session->userdata('companyid')));
		        $access=array();
		        $permit=array();
		        $permit=array_column($permission,'permission_id');
		        foreach ($rolepermit as $key => $value) {
		            if(in_array($value['permission_id'],$permit)) {
		              $key_to_access=array_search($value['permission_id'],$permit);
		              $access[]=$permission[$key_to_access]['permission_id'];
		            }
		        }
		        return $access;
		    }
		    else {
		        return false;
		    }
		}	
	*/
	/* 
		Generate HTTP request
		accept 1 parameter
		1 - url of page.	
	*/
	function httpRequest($url) {
	    $pattern = "/http...([0-9a-zA-Z-.]*).([0-9]*).(.*)/";
	    preg_match($pattern,$url,$args);
	    $in = "";
	    $fp = fsockopen($args[1],80, $errno, $errstr, 30);
	    if (!$fp) {
	    	return("$errstr ($errno)");
	    } 
	    else {
	  		$args[3] = "C".$args[3];
	        $out = "GET /$args[3] HTTP/1.1\r\n";
	        $out .= "Host: $args[1]:$args[2]\r\n";
	        $out .= "User-agent: PARSHWA WEB SOLUTIONS\r\n";
	        $out .= "Accept: */*\r\n";
	        $out .= "Connection: Close\r\n\r\n";

	        fwrite($fp, $out);
	        while (!feof($fp)) {
	        	$in.=fgets($fp, 128);
	        }
	    }
	    fclose($fp);
	    return($in);
	}		

	/* 
		Convert Amount to word 
		accept 1 parameter
		1 - integer to be converted into words.		
	*/
	function number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			// overflow
			trigger_error(
				'number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
			);
			return false;
		}

		if ($number < 0) {
			return $negative . number_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}

		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
			break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . number_to_words($remainder);
				}
			break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= number_to_words($remainder);
				}
			break;
		}

		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}

		return strtoupper($string);
	}	

	function get_indian_currency($number) {
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);
		$i = 0;
		$str = array();
		$words = array(0 => '', 1 => 'one', 2 => 'two',
			3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
			7 => 'seven', 8 => 'eight', 9 => 'nine',
			10 => 'ten', 11 => 'eleven', 12 => 'twelve',
			13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
			16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
			19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
			40 => 'forty', 50 => 'fifty', 60 => 'sixty',
			70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
		$digits = array('', 'hundred','thousand','lakh', 'crore');
		while( $i < $digits_length ) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} else $str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		$paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
		return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
	}

	/* 
		Validation rules for controls.
		accept 1 parameter
		1 - value of control.
		2 - define rules for HTML control.
	*/
	function validate($value,$rule) {
		if($rule=="") {
			return true;
		}
		$return=false;
		$arr_value=explode("|", $rule);
		foreach($arr_value as $case_value) {	
			switch($case_value) {
				case "required": 
					if ($value=="") { 
						$return=false;
					} 
					else { 
						$return=true;
					};
				break;
				case "valid_email": 
					if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
						$return=true;
					}
					else {
						$return=false;
					}
				break;
				case "mobile": 
					if(preg_match('/^\d{10}$/',$value)) {
						$return=true;
					}
					else {
						$return=false;
					}
				break;			
				case "numeric":
					if (is_numeric($value)) {
						$return=true;
					} 
					else {
						$return=false;
					}
				break;	
				default: 
					$return=true;

			}
		}
		return $return;
	}	

	/**
	 * This file is part of the array_column library
	 *
	 * For the full copyright and license information, please view the LICENSE
	 * file that was distributed with this source code.
	 *
	 * @copyright Copyright (c) 2013 Ben Ramsey <http://benramsey.com>
	 * @license http://opensource.org/licenses/MIT MIT
	 */

	if (!function_exists('array_column')) {

	    /**
	     * Returns the values from a single column of the input array, identified by
	     * the $columnKey.
	     *
	     * Optionally, you may provide an $indexKey to index the values in the returned
	     * array by the values from the $indexKey column in the input array.
	     *
	     * @param array $input A multi-dimensional array (record set) from which to pull
	     *                     a column of values.
	     * @param mixed $columnKey The column of values to return. This value may be the
	     *                         integer key of the column you wish to retrieve, or it
	     *                         may be the string key name for an associative array.
	     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
	     *                        the returned array. This value may be the integer key
	     *                        of the column, or it may be the string key name.
	     * @return array
	     */
	    function array_column($input = null, $columnKey = null, $indexKey = null) {
	        // Using func_get_args() in order to check for proper number of
	        // parameters and trigger errors exactly as the built-in array_column()
	        // does in PHP 5.5.
	        $argc = func_num_args();
	        $params = func_get_args();

	        if ($argc < 2) {
	            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
	            return null;
	        }

	        if (!is_array($params[0])) {
	            trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
	            return null;
	        }

	        if (!is_int($params[1])
	            && !is_float($params[1])
	            && !is_string($params[1])
	            && $params[1] !== null
	            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
	        ) {
	            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
	            return false;
	        }

	        if (isset($params[2])
	            && !is_int($params[2])
	            && !is_float($params[2])
	            && !is_string($params[2])
	            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
	        ) {
	            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
	            return false;
	        }

	        $paramsInput = $params[0];
	        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

	        $paramsIndexKey = null;
	        if (isset($params[2])) {
	            if (is_float($params[2]) || is_int($params[2])) {
	                $paramsIndexKey = (int) $params[2];
	            } else {
	                $paramsIndexKey = (string) $params[2];
	            }
	        }

	        $resultArray = array();

	        foreach ($paramsInput as $row) {

	            $key = $value = null;
	            $keySet = $valueSet = false;

	            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
	                $keySet = true;
	                $key = (string) $row[$paramsIndexKey];
	            }

	            if ($paramsColumnKey === null) {
	                $valueSet = true;
	                $value = $row;
	            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
	                $valueSet = true;
	                $value = $row[$paramsColumnKey];
	            }

	            if ($valueSet) {
	                if ($keySet) {
	                    $resultArray[$key] = $value;
	                } else {
	                    $resultArray[] = $value;
	                }
	            }

	        }

	        return $resultArray;
	    }
	}

	function get_pagination_config() {
	    $CI=&get_instance();
	    $CI->config->load('pagination', TRUE);
	    return $CI->config->config['pagination'];
	}

	function quotation_status() {
		$quotation_status=array(
			array(
				'key'=>'Draft',
				'value'=>'Draft'
			),
			array(
				'key'=>'Sent',
				'value'=>'Sent'
			),
			array(
				'key'=>'Approved',
				'value'=>'Approved'
			),
			array(
				'key'=>'Reject',
				'value'=>'Reject'
			),
		);
	    return $quotation_status;
	}
	
	/* Round of number
		1.val
		The value to round

		2.precision
		The optional number of decimal digits to round to.

		3.mode
		Use one of the following constants to specify the mode in which rounding occurs.
	*/
	function round_amount($amount, $precision=false,  $mode=false) {
		$round_amount = round($amount, $precision, $mode);
		return $round_amount;
	}

	/* 
		Return Financial Year of date
		$status - start for starting year
		        - end for end year
		$date - Date 
		$year_start - Financial Year start month
	*/
	function get_finacialyear($status=false, $date=false, $year_start=4) {
	    if(!$date) {
	      $date=date('Y-m-d');
	    }
	    $time=strtotime($date);
	    $month=date('m',$time);
	    $current_year=date('Y',$time);
	    $current_yr=date('y',$time);
	    if($month >= $year_start) {
	    	if($status=='start') {
	    		return $current_year;
	    	}
	    	if($status=='end') {
	    		return ($current_year+1);
	    	}
	      	return $current_year.'-'.($current_yr+1);
	    }
	    else {
	    	if($status=='start') {
	    		return ($current_year-1);
	    	}
	    	if($status=='end') {
	    		return $current_year;
	    	}
	      	return ($current_year-1).'-'.$current_yr;
	    }
	}

	function invoice_type() {
		$invoice_type=array(
			array(
				'key'=>'Import',
				'value'=>'Import'
			),
			array(
				'key'=>'Export',
				'value'=>'Export'
			)
		);
	    return $invoice_type;
	}

	/* 
		$permission_id to check in session 
			@type mixed array or integer
		$check_type
			@string
			AND - User must have all the permission from provided 
			OR - User must have any one permission from list
		$redirect_back
			@type Integer
			0 - Return true/ false 
			1 - Redirect to back page
			2 - echo html formatted error message and stop execution
			3 - Return json formatted error message and die

		@Return true on permission found and above action from 0,1,2 if permission not exist

	*/
	function permission_check($permission_machine, $check_type='and', $redirect_back=1) {
		check_loggedin($redirect_back);
		$CI=&get_instance();
		$role_id=$CI->session->userdata('role_id');

		$user_permission=$CI->session->userdata('permission_machine');

		if(empty($user_permission)) {
			return redirect_action($redirect_back);
		}
		
		/* If user have system admin permission, Always return true */
		if(in_array('10', $user_permission)) {
			return true;
		}

		/* Check multiple permission */
		if(is_array($permission_machine)) {
			$available_permission=false;
			$unavailable_permission=false;

			/* Checking permission */
			foreach ($permission_machine as $permission) {
				if(in_array($permission, $user_permission)) {
					$available_permission=true;
				}
				else {
					$unavailable_permission=true;
				}
			}

			/* when single permission required */
			if(strtolower($check_type) == 'or') {
				if($available_permission) {
					return true;	
				}
				else {
					redirect_action($redirect_back);
				}
				
			}
			/* When all permission required */
			else {
				if($available_permission && $unavailable_permission===false) {
					return true;
				}
				else {
					redirect_action($redirect_back);
				}
			}
		}
		/* Check single permission */
		else {
			if(in_array($permission_machine, $user_permission)) {
				return true;
			}
			else {
				redirect_action($redirect_back);
			}
		}
	}

	function redirect_action($redirect_back=1, $redirect_url=false, $error_msg="You don't have sufficient permission to access this resource.") {
		if($redirect_back==0) {
            return false;
		}
		elseif($redirect_back==1) {
			set_message($error_msg);
			if($redirect_url == false) {
				redirect_back();die;
			}
			else {
				redirect($redirect_url);
			}
		}
		elseif($redirect_back==2) {
			echo '<div class="alert alert-danger alert-dismissable">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						'.$error_msg.'
				</div>';
			die;
		}
		elseif($redirect_back==3) {
			echo json_encode(array("status"=>0,'message'=>$error_msg));
			die;
		}
		else{
			return false;
		}
	}


?>