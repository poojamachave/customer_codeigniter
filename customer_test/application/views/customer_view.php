
<!DOCTYPE html>
<html>
<head>
	<!--- bootstrap & css link -->
	<title>Customer Form</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/customer.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<a href="<?php echo base_url() . "Customer_controller/edit_customer/"  ?>"></a>

  
	<?php if(!empty($customer_id))  { foreach ($customer_id as $customer) { }} ?>
	<div class="container_fluid">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Customer</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="Customer_controller/customer">Add User</a></li>
					<li><a href="Customer_controller/show_customer">View User</a></li>
					<li><a href="Customer_controller/logout">Logout</a></li>
				</ul>
			</div>
		</nav>
	</div>
	<?php  if(!empty($this->session->flashdata("error"))) {
         echo '<center><div class="alert alert-danger" style="width:50em;">'.$this->session->flashdata("error").'</button></div>';  
    }
    ?>
	<div class="container" id="form_container">
		<center><h3>Customer Form</h3></center>
		<form method="POST" name="files" action="<?php echo base_url('Customer_controller/customer_form_validation'); ?>" id="customer_form" enctype="multipart/form-data">
			<?php
				if(!empty($customer)) {
					echo '<input type="hidden" name="edit_id" value="'.$customer->customer_id.'"/>';
				}
			?>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="name">Name*</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="name" name="name" placeholder="Enter name here"  data-validation="required" value="<?php if(!empty($customer->name)) { echo $customer->name; }?>
						<?php 
							if(!empty($_SESSION['old_post']['name'])) {
								 echo $_SESSION['old_post']['name'];unset($_SESSION['old_post']['name']);
							}
						?>"
					/>
					<span class="error">
						<?php echo form_error('name');?>
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="company name">Company Name*</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter Company Name here"  data-validation="required"  value="
					<?php if(!empty($customer->company_name)) { 
						echo $customer->company_name; 
					} 
					else {  
						if(!empty($_SESSION['old_post']['company_name'])) { 
							echo $_SESSION['old_post']['company_name']; 
							unset($_SESSION['old_post']['company_name']);
						}
					}
					?>" 
					/>
					<span class="error">
						<?php echo form_error('company_name');?>
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="mobile no">Mobile no*</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="mobile_no" name="mobile"  placeholder="Enter Mobile no here"  data-validation="required|number" data-validation-length="max10" value="<?php if(!empty($customer->mobile_no))  { echo $customer->mobile_no; } ?>"/>
					<span class="error">
						<?php echo form_error('mobile_no');?>
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="birth date">Date Of Birth*</label>
				<div class="col-sm-9">
					<input type="date" class="form-control" name="birthdate"  placeholder="Enter Birth Date here"  data-validation="required|birthdate" data-validation-help="yyyy-mm-dd" value="<?php if(!empty($customer->birthdate)) { echo $customer->birthdate; }?>"/>
					<span class="error">
						<?php echo form_error('birthdate');?>
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="hobbies">Hobbies*</label>
					<div class="col-sm-9">
						<select class="form-control dropdown" name="hobbies[]"  data-validation="required" size="3" multiple="multiple" />
							<?php if(!empty($customer->hobbies)) { $hobby=explode(',', $customer->hobbies); }?>
							<option value="reading" 
							<?php if(!empty($hobby) && in_array('reading', $hobby)){ echo 'selected';} ?> 
							>Reading
							</option>
							<option value="writing" 
							<?php if(!empty($hobby) && in_array('writing', $hobby)) { echo 'selected';} ?> 
							>Writing
							</option>
							<option value="drawing" 
							<?php if(!empty($hobby) && in_array('drawing', $hobby)){ echo 'selected';} ?> 
							>Drawing
							</option>
						</select>
						<span class="error">
							<?php echo form_error('hobbies');?>
						</span>
					</div>
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="email">Email*</label>
				<div class="col-sm-9">
					<input type="email"  class="form-control" id="email"  name="email"  placeholder="mail@gmail.com" data-validation="required|email" value="<?php  if(!empty($customer->email)) { echo $customer->email; } ?>"/>
					<span class="error">
						<?php echo form_error('email');?>
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="status">Status*</label>
				<div class="col-sm-9">
					<label><input type="radio" name="status" value="enable" data-validation="required"
						<?php 
						if(!empty($customer->status) && $customer->status=="enable") {
							echo 'checked';
						}
						else {
								if(!empty($_SESSION['old_post']['status']) && $_SESSION['old_post']['status']=="enable" ) {
									echo 'checked';
									unset($_SESSION['old_post']['status']);
								}
						}
						?>>enable
					</label>&nbsp;&nbsp;
					<label><input type="radio" name="status" value="disable" data-validation="required"
						<?php 
						if(!empty($customer->status) && $customer->status=="disable") {
							echo 'checked';
						}
						else {
								if(!empty($_SESSION['old_post']['status']) && $_SESSION['old_post']['status']=="disable" ) {
									echo 'checked';
									unset($_SESSION['old_post']['status']);
								}
						}
						?>>disable
					</label>
					<span class="error">
						<?php echo form_error('status');?>
					</span>
				</div>
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="gender">Gender*</label>
					<div class="col-sm-9">
						<label><input type="radio" name="gender" value="male"
							<?php 
							if(!empty($customer->gender) && $customer->gender=="male") {
								echo 'checked';
							}
							else {
								if(!empty($_SESSION['old_post']['gender']) && $_SESSION['old_post']['gender']=="male" ) {
									echo 'checked';
									unset($_SESSION['old_post']['gender']);
								}
							}
							?>  data-validation="required">Male
						</label>&nbsp;&nbsp;
						<label><input type="radio" name="gender" value="female"
							<?php 
							if(!empty($customer->gender) && $customer->gender=="female") {
								echo 'checked';
							}
							else {
								if(!empty($_SESSION['old_post']['gender']) && $_SESSION['old_post']['gender']=="female") {
									echo 'checked';
									unset($_SESSION['old_post']['gender']);
								}
							}
							?>  data-validation="required">Female
						</label>
						<span class="error">
							<?php echo form_error('gender');?>
						</span>
					</div>
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="password">Password*</label>
				<div class="col-sm-9">
					<input type="password" class="form-control" id="password" name="password" placeholder="Enter password here"   data-validation="required" value="<?php if(!empty($_SESSION['old_post']['password'])) {
									echo $_SESSION['old_post']['password'];unset($_SESSION['old_post']['password']);
								}?>"/>
					<span class="error">
						<span class="error">
							<?php echo form_error('password');?>
						</span>
					</span>
				</div>	
			</div><br>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="Confirmpassword">Confirm Password*</label>
				<div class="col-sm-9">
					<input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Enter Confirm password here"   data-validation="required" value=""/>
					<span class="error">
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="city">City*</label>
				<div class="checkbox col-sm-9">
					<?php  if(!empty($customer->city)) { $city_array=explode(',', $customer->city); }?>
					<label><input type="checkbox"  name="city[]" value="Mumbai"
						<?php 
							if(!empty($city_array) && in_array('Mumbai', $city_array)) {
								echo 'checked';
							}
						?>  data-validation="required">Mumbai</label>
					<label><input type="checkbox"  name="city[]" value="Solapur"
						<?php 
							if(!empty($city_array) && in_array('Solapur', $city_array)) {
								echo 'checked';
							}
						?>  data-validation="required">Solapur</label>
					<label><input type="checkbox"  name="city[]" value="Delhi" 
						<?php 
							if(!empty($city_array) && in_array('Delhi', $city_array)) {
								echo 'checked';
							}
						?>  data-validation="required">Delhi</label>
					<span class="error">
						<?php echo form_error('city');?>
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="address">Address*</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="address"  name="address" placeholder="Enter Address here" data-validation="required" value="<?php if(!empty($customer->address)) { echo $customer->address; }?>"/>
					<span class="error">
						<?php echo form_error('address');?>
					</span>
					
				</div>
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="zip">Zip*</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="zip" name="zip" placeholder="Enter zip here" data-validation="required" value="<?php if(!empty($customer->zip)) { echo $customer->zip; }?>"/>
					<span class="error">
						<?php echo form_error('zip');?>
					</span>
				</div>	
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="State">State*</label>
					<div class="col-sm-9">
						<select class="form-control dropdown" name="state" data-validation="required">
							<option>Select States</option>
							<option value="maharashtra" <?php if(!empty($customer->state) && $customer->state=="maharashtra"){ echo 'selected';}?>>Maharashtra</option>
							<option value="goa"  <?php if(!empty($customer->state) && $customer->state=="goa"){ echo 'selected';}?>>Goa</option>
							<option value="andhra pradesh"  <?php if(!empty($customer->state) && $customer->state=="andhra pradesh"){ echo 'selected';}?>>Andhra Pradesh</option>
							<option value="assam"  <?php if(!empty($customer->state) && $customer->state=="assam"){ echo 'selected';}?>>Assam</option>
							<option value="bihar"  <?php if(!empty($customer->state) && $customer->state=="bihar"){ echo 'selected';}?>>Bihar</option>
						</select>
						<span class="error">
							<?php echo form_error('state');?>
						</span>
					</div>
			</div>
			<div class="form-group col-md-8">
				<label class="control-label col-sm-3" for="upload">Upload Image*</label>
				<div class="col-sm-9">
					<input type="file"  id="upload_img" name="file_upload" data-validation="required" value="upload"/>
					<?php if(!empty($customer->uploads)) { ?>
					<img class="thumbnail" id="image_customer" src="<?php  if(!empty($customer->uploads)) { echo $customer->uploads; }?>" height="200px" width="200px"/>
					<a class="delete_img"><span class="glyphicon glyphicon-trash" id="trash_img"></span></a>
					<?php } ?>
				</div>	
				<span class="error">	
				</span>
			</div>
			<div class="form-group col-md-8" id="btn-style">
				<input type="submit" name="submit" value="submit" class="btn btn-info" id="submit">
			</div>
		</form>
	</div>

<!---jquery first and then bootstrap js-->
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
	  $.validate({
	    modules : 'html5'
	  });
</script>
<script>
$.validate({
  modules : 'date'
});
</script>
<script type="text/javascript">
	$(document).on('click','.delete_img',function() {
		$(this).parent().remove();
		$('#customer_form').append('<input type="hidden" name="deleteimg" value="1">');
	});	
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap.min.js"></script>
</body>
</html>
