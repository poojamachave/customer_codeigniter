<!DOCTYPE html>
<html>
<head>
   <title>login</title>
   <link rel="stylesheet" type = "text/css" href="<?php echo base_url(); ?> css/bootstrap.css">
   <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
</head>
<body>
<div class="main-form">
   <div class="container">
      <div class="row">
         <div class="col-md-12 pd">
            <div class="form-box box-shadow">
               <div class="heading text-center">
                  <h1>Customer Login</h1>
               </div>
               <div class="form-here">
                  <form method="POST" action="<?php echo base_url('Customer_controller/login_validation');?>">
                     <div class="form-group">
                        <?php  
                          echo '<label class="text-danger">'.$this->session->flashdata("error").'</label>';  
                        ?>  
                        <div class="input-box">
                           <input type="text" class="form-control mrb" id="email" placeholder="Email" name="email">
                           <span class="glyphicon glyphicon-user form-glyphicon"></span>
                           <span class="text-danger"><?php echo form_error('email'); ?></span>  
                        </div>
                        <div class="input-box">
                           <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                           <span class="glyphicon glyphicon-lock form-glyphicon"></span>
                           <span class="text-danger"><?php echo form_error('password'); ?></span>
                        </div>
                     </div>
                     <button type="submit" class="btn btn-primary btn-block">Login</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</body>
<script type = 'text/javascript' src = "<?php echo base_url(); ?>js/jquery.js"></script>
<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
</html>