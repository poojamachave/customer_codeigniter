
<!DOCTYPE html>
<html>
<body>
  <head>
    <link rel="stylesheet" type = "text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/customer.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head> 
<div class="container_fluid">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Customer</a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="Customer_controller/customer">Add User</a></li>
                    <li><a href="Customer_controller/show_customer">View User</a></li>
                    <li><a href="Customer_controller/logout">Logout</a></li>
                </ul>
            </div>
        </nav>
  
    <?php  
      if(!empty($this->session->flashdata("success_msg"))) {
        echo '<center><button class="alert alert-success">'.$this->session->flashdata("success_msg").'</button></center>';  
     }
    ?> 
    <?php  
      if(!empty($this->session->flashdata("update_msg"))) {
        echo '<center><button class="alert alert-success">'.$this->session->flashdata("update_msg").'</button></center>';
      }
    ?>
    <?php
        foreach($customers as $customer) { }
    ?> 
      <div style="margin-left:100em;"><?php if (isset($pagination)) { echo $pagination; }?></div> 
  

    <form action="<?php echo base_url('/Customer_controller/show_customer');?>" method="GET">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" id="search_btn">search</button>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Customer</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="recipient-name" class="control-label">Keyword</label>
                <input type="text" class="form-control" id="recipient-name" name="keyword">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">Hobbies</label>
                <select class="form-control dropdown" name="hobby"/>
                  <option value="">~~select hobby ~~</option>
                  <option value="reading">Reading</option>
                  <option value="writing" >Writing</option>
                  <option value="drawing">Drawing</option>
                </select>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">Status</label>
                <label><input type="radio" name="status" value="enable">enable</label>&nbsp;&nbsp;
                <label><input type="radio" name="status" value="disable">disable</label>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">Gender</label>
                <label><input type="radio" name="gender" value="male">Male</label>&nbsp;&nbsp;
                <label><input type="radio" name="gender" value="female">Female</label>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">City</label>
                <label><input type="checkbox"  name="city" value="Mumbai">Mumbai</label>
                <label><input type="checkbox"  name="city" value="Solapur">Solapur</label>
                <label><input type="checkbox"  name="city" value="Delhi">Delhi</label>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">State</label>
                <select class="form-control dropdown" name="state"/>
                  <option>~~ select state~~</option>
                  <option value="maharashtra">Maharashtra</option>
                  <option value="goa" >Goa</option>
                  <option value="andhra pradesh">Andhra pradesh</option>
                  <option value="assam" >Assam</option>
                  <option value="bihar" >Bihar</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" value="search" class="btn btn-primary" name="search"></button>
          </div>
        </div>
      </div>
    </div>
   </form>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Customer_id</th>
        <th>Name</th>
        <th>Company Name</th>
        <th>Mobile no</th>
        <th>Birthdate</th>
        <th>Hobbies</th>
        <th>Email ID</th>
        <th>Status</th>
        <th>Gender</th>
        <th>Password</th>
        <th>City</th>
        <th>Address</th>
        <th>Zip</th>
        <th>State</th>
        <th>Uploads</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($customers as $customer)  {  ?>
      <tr> 
        <td><?php echo $customer->customer_id;?></td>
        <td><?php echo $customer->name;?></td>
        <td><?php echo $customer->company_name;?></td>
        <td><?php echo $customer->mobile_no;?></td>
        <td><?php echo $customer->birthdate;?></td>
        <td><?php echo $customer->hobbies;?></td>
        <td><?php echo $customer->email;?></td>
        <td><?php echo $customer->status;?></td>
        <td><?php echo $customer->gender;?></td>
        <td><?php echo $customer->password;?></td>
        <td><?php echo $customer->city;?></td>
        <td><?php echo $customer->address;?></td>
        <td><?php echo $customer->zip;?></td>
        <td><?php echo $customer->state;?></td>
        <td><?php echo $customer->uploads;?></td>
        <td><a href="edit_customer?customer_id=<?php echo $customer->customer_id; ?>"><button class="btn btn-primary">edit</button></a></td>
        <td><a href="customer_id_delete?customer_id=<?php echo $customer['customer_id']; ?>" data-toggle="modal" onClick="return confirm('you want to delete this record');"><button class="btn btn-primary">delete</button></a></td>
        <td></td>
      </tr>
    <?php } ?>
    </tbody>
  </table> 
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"  crossorigin="anonymous"></script>
</body>
</html>